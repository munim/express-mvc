/**
 * Created by 605894141 on 28/10/2014.
 */

var _ = require("lodash");
var pathToRegexp = require("path-to-regexp");

module.exports = function (app, ctrlPath) {
    var setupController = function (ctrl) {
        var REGEX_NAME = /([A-Z]+)(.*?)$/i;

        var FN_ARGS = /^function\s*[^\(]*\(\s*([^\)]*)\)/m;
        var FN_ARG_SPLIT = /,/;
        var FN_ARG = /^\s*(_?)(.+?)\1\s*$/;
        var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

        function getFunctionParams(fn) {
            var params = [];
            var fnText = fn.toString().replace(STRIP_COMMENTS, '');
            var argDecl = fnText.match(FN_ARGS);
            _.forEach(argDecl[1].split(FN_ARG_SPLIT), function (arg) {
                arg.replace(FN_ARG, function (all, underscore, name) {
                    params.push(name);
                });
            });

            return params;
        }

        for (var key in ctrl) {
            if (ctrl.hasOwnProperty(key)) {
                var matches = REGEX_NAME.exec(key);

                if (matches.length === 3) {
                    var val = ctrl[key];

                    var middlewares = null;
                    var fn = null;

                    if (_.isArray(val)) {
                        middlewares = val.slice(0, val.length - 1);
                        fn = val.slice(val.length - 1)[0];
                    }
                    else if (_.isFunction(val)) {
                        fn = val;
                    }

                    var params = getFunctionParams(fn);

                    var keys = [];
                    var re = pathToRegexp(matches[2], keys);


                    var actionType = matches[1].toLowerCase();
                    app[actionType](matches[2], function (req, res, next) {
                        var callbackParam = [];
                        _.forEach(params, function (param) {
                            if (param === "form" && actionType === "post") {
                                callbackParam.push(request.body);
                            }
                            else {
                                callbackParam.push(req.query[param]);
                            }
                        });

                    });
                }
            }
        }
    }
    var ctrlObj = require(ctrlPath);
    setupController(ctrlObj);
}